# Copyright 2018 raykzhao and others
# Distributed under the terms of the GNU General Public License v2

EAPI="6"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~amd64 ~x86"
HOMEPAGE="https://github.com/raykzhao/raykzhao-overlay"

DESCRIPTION="Raykzhao's kernel sources"

SRC_URI="${KERNEL_URI}"

src_prepare() {
	eapply "${FILESDIR}"/gentoo-patches/
	eapply "${FILESDIR}"/0004-arch-Kconfig-Default-to-maximum-amount-of-ASLR-bits.patch
	eapply "${FILESDIR}"/zen.patch
	eapply "${FILESDIR}"/polly.patch
	eapply "${FILESDIR}"/zen_mm.patch
	eapply "${FILESDIR}"/0001-linux6.12.3-le9uo-1.10.patch
	eapply "${FILESDIR}"/lru_drain.patch
	eapply "${FILESDIR}"/c9dda55c5e6834c384a63d5974f894c62bc2f6be.patch
	eapply "${FILESDIR}"/0001-ADIOS-v1.5.3.patch
	eapply "${FILESDIR}"/pid_v6.patch
	eapply "${FILESDIR}"/crypto-x86---minor-optimizations-and-cleanup-to-VAES-code.patch
	default
}
